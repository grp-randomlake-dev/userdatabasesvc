package com.randomlake.userdatabasesvc;

import com.randomlake.userdatabasesvc.model.User;
import com.randomlake.userdatabasesvc.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserdatabasesvcApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(UserdatabasesvcApplication.class, args);
	}

	@Autowired
	private UserRepository userRepository;

	@Override
	public void run(String... args) throws Exception {
		this.userRepository.save(new User("Tom", "Cruise", "tc@randomlake.com"));
		this.userRepository.save(new User("Tommy", "Stark", "ts@randomlake.com"));
		this.userRepository.save(new User("Tamara", "Webb", "tw@randomlake.com"));
	}

}
