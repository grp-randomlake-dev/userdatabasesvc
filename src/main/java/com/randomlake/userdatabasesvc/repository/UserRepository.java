package com.randomlake.userdatabasesvc.repository;

import com.randomlake.userdatabasesvc.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
