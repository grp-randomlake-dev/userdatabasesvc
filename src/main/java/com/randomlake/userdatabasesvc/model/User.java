package com.randomlake.userdatabasesvc.model;

import jakarta.persistence.*;
//Add annotations to make it a JPA entity
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "first_name") // Using the annotation allows you to map to the db column name
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    // No annotation needed because the database column name is the same as here
    private String email;

    // Create default constructor
    public User() {

    }

    // Generate constructors
    public User(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    // generate getters and setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
